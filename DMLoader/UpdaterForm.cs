﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DMLoader.Controllers;
using DMLoader.Controllers.Events;
using DMLoader.Helpers;
using DMLoader.Updater;

namespace DMLoader
{
   public partial class UpdaterForm : Form {

      private AppController _controller;

      public UpdaterForm()
      {

         _controller = new AppController();


         InitializeComponent();
         SetupHandlers();
         BindControls();

         Thread thread = new Thread(() => {
            IBehavior behavior = new CommandLinePatchingBehavior(_controller);
            behavior.Action();
         });

         thread.Start();

      }

      private void SetupHandlers() {

         _controller.FileDownloadProgressChanged += FileDownloadProgressChanged;
         _controller.NoUpdatesAvailable += (sender, args) => statusLabel.Text = "No updates available";
         _controller.FileDownloadBegan += FileDownloadBegan;
         _controller.FileUnzipped += FileUnzipped;
      }

      private void FileDownloadProgressChanged(object o, FileDownloadProgressEventArgs fileDownloadProgressEventArgs)
      {
         UI.SetProgressBarProgress(statusStrip1, progressBar, fileDownloadProgressEventArgs.Percentage);
         UI.SetPbValue(downloadProgress, fileDownloadProgressEventArgs.Percentage);
      }

      private void BindControls()
      {
         TitleLabel.Text = _controller.Settings.UpdaterTitle;
         this.Text = _controller.Settings.UpdaterTitle;
      }

      private void FileUnzipped(object o, FileUnzipEventArgs fileUnzipEventArgs) {
         UI.SetControlText(statusStrip1, statusLabel, $"Rozpakowywanie {fileUnzipEventArgs.FileName}");
      }

      private void FileDownloadBegan(object o, FileDownloadBeginEventArgs fileDownloadBeginEventArgs) {
         UI.SetControlText(statusStrip1, statusLabel, $"Pobieranie {fileDownloadBeginEventArgs.FileName}");
      }

      private void ustawieniaToolStripMenuItem_Click(object sender, EventArgs e)
      {
         var sf = new SettingsForm(_controller);
         sf.ShowDialog();
      }

      private void quitToolStripMenuItem_Click(object sender, EventArgs e)
      {
         Application.Exit();
      }

      private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
      {
         MessageBox.Show("Patcher UO. http://bitbucket.org/goqsane/");
      }

      private void button1_Click(object sender, EventArgs e)
      {
         Process.Start(_controller.Settings.HttpUri);
      }
   }
}
