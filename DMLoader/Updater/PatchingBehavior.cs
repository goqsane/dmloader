﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMLoader.Controllers;
using DMLoader.Controllers.Events;

namespace DMLoader.Updater
{
   public class UiPatchingBehavior : IBehavior
   {

      private readonly AppController _controller;

      public EventHandler<FileDownloadBeginEventArgs> FileDownloadBegan { get; }
      public EventHandler<FileUnzipEventArgs> FileUnzipped { get; }

      public string Name => "Patching";

      public UiPatchingBehavior(AppController controller)
      {
         _controller = controller;
      }

      public void Action()
      {
         
      }
   }
}
