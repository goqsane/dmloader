﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMLoader.Controllers;
using DMLoader.Controllers.Events;
using SharpCompress.Archives;
using SharpCompress.Archives.Zip;
using SharpCompress.Readers;

namespace DMLoader.Updater
{
   public class CommandLinePatchingBehavior : IBehavior {

      private readonly AppController _controller;


      public string Name => "cmd";

      public CommandLinePatchingBehavior(AppController controller) {
         _controller = controller;
      }

      public void Action() {

         var patchingFolder = Path.Combine(_controller.Settings.UoPath, "dmpatcher");

         if (!Directory.Exists(patchingFolder)) {
            Directory.CreateDirectory(patchingFolder);
         }

         if (_controller.UpdatesAvailable()) {

            foreach (var file in _controller.GetUpdateFile().Downloads) {
               try {
                  _controller.FileDownloadBegan?.Invoke(this, new FileDownloadBeginEventArgs(file.Name, file.Url));
                  _controller.DownloadFile(new Uri(file.Url), Path.Combine(patchingFolder, file.Name));
                  _controller.DownloadFileDownloadEnd?.Invoke(this, EventArgs.Empty);

                  if (file.RequiresUnzipping) {

                     _controller.FileUnzipped?.Invoke(this, new FileUnzipEventArgs(file.Name, _controller.Settings.UoPath));

                     FileInfo fi = new FileInfo(Path.Combine(patchingFolder, file.Name));
                     ZipArchive zipFile = ZipArchive.Open(fi);
                     foreach (var entry in zipFile.Entries) {
                        entry.WriteToDirectory(_controller.Settings.UoPath,
                           new ExtractionOptions() {Overwrite = true, ExtractFullPath = true});
                     }
                  }

               }
               catch (Exception ex) {
                  throw;
               }
            }

            // after all updates are done, we run the custom patching utility

            ProcessStartInfo patchingUtility = new ProcessStartInfo(Path.Combine(_controller.Settings.UoPath, _controller.Settings.PatchingUtilityName));
            patchingUtility.WindowStyle = ProcessWindowStyle.Hidden;
            patchingUtility.RedirectStandardInput = true;
            Process patchingProcess = new Process() {StartInfo = patchingUtility};
            patchingProcess.Start();
            // wait for 5 seconds to do user input first
            System.Threading.Thread.Sleep(5000);
            patchingProcess.StandardInput.WriteLine(_controller.Settings.PatchingUtilityInteractions);
            System.Threading.Thread.Sleep(5000);
            while (!patchingProcess.HasExited)
            {
               // we press enter every second until the program exited.
               System.Threading.Thread.Sleep(1000);
               patchingProcess.StandardInput.WriteLine("");
            }

            _controller.CustomToolRan?.Invoke(this, EventArgs.Empty);




         }
      }
   }
}
