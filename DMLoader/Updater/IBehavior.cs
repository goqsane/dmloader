﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMLoader.Controllers;
using DMLoader.Controllers.Events;

namespace DMLoader.Updater
{
   interface IBehavior
   {
      string Name { get; }
      void Action();
   }
}
