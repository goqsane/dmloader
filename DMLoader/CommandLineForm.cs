﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMLoader.Controllers;
using DMLoader.Updater;

namespace DMLoader
{
   public class CommandLineForm
   {
      public CommandLineForm() {

         AppController controller = new AppController();
         IBehavior behavior = new CommandLinePatchingBehavior(controller);
         behavior.Action();
      }
   }
}
