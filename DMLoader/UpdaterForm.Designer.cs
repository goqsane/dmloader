﻿namespace DMLoader
{
   partial class UpdaterForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdaterForm));
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.ustawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.TitleLabel = new System.Windows.Forms.Label();
         this.statusStrip1 = new System.Windows.Forms.StatusStrip();
         this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
         this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
         this.uoButton = new System.Windows.Forms.Button();
         this.button1 = new System.Windows.Forms.Button();
         this.downloadProgress = new System.Windows.Forms.ProgressBar();
         this.menuStrip1.SuspendLayout();
         this.statusStrip1.SuspendLayout();
         this.SuspendLayout();
         // 
         // menuStrip1
         // 
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.pomocToolStripMenuItem});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Size = new System.Drawing.Size(427, 24);
         this.menuStrip1.TabIndex = 0;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // plikToolStripMenuItem
         // 
         this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ustawieniaToolStripMenuItem,
            this.quitToolStripMenuItem});
         this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
         this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
         this.plikToolStripMenuItem.Text = "&Plik";
         // 
         // ustawieniaToolStripMenuItem
         // 
         this.ustawieniaToolStripMenuItem.Name = "ustawieniaToolStripMenuItem";
         this.ustawieniaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
         this.ustawieniaToolStripMenuItem.Text = "&Ustawienia";
         this.ustawieniaToolStripMenuItem.Click += new System.EventHandler(this.ustawieniaToolStripMenuItem_Click);
         // 
         // quitToolStripMenuItem
         // 
         this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
         this.quitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
         this.quitToolStripMenuItem.Text = "Exit";
         this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
         // 
         // pomocToolStripMenuItem
         // 
         this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramieToolStripMenuItem});
         this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
         this.pomocToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
         this.pomocToolStripMenuItem.Text = "P&omoc";
         // 
         // oProgramieToolStripMenuItem
         // 
         this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
         this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
         this.oProgramieToolStripMenuItem.Text = "O programie";
         this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.oProgramieToolStripMenuItem_Click);
         // 
         // TitleLabel
         // 
         this.TitleLabel.AutoSize = true;
         this.TitleLabel.Location = new System.Drawing.Point(12, 29);
         this.TitleLabel.Name = "TitleLabel";
         this.TitleLabel.Size = new System.Drawing.Size(12, 15);
         this.TitleLabel.TabIndex = 2;
         this.TitleLabel.Text = "x";
         // 
         // statusStrip1
         // 
         this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.progressBar});
         this.statusStrip1.Location = new System.Drawing.Point(0, 145);
         this.statusStrip1.Name = "statusStrip1";
         this.statusStrip1.Size = new System.Drawing.Size(427, 22);
         this.statusStrip1.TabIndex = 3;
         this.statusStrip1.Text = "statusStrip1";
         // 
         // statusLabel
         // 
         this.statusLabel.Name = "statusLabel";
         this.statusLabel.Size = new System.Drawing.Size(0, 17);
         // 
         // progressBar
         // 
         this.progressBar.Name = "progressBar";
         this.progressBar.Size = new System.Drawing.Size(400, 16);
         this.progressBar.Step = 1;
         // 
         // uoButton
         // 
         this.uoButton.Location = new System.Drawing.Point(15, 57);
         this.uoButton.Name = "uoButton";
         this.uoButton.Size = new System.Drawing.Size(178, 62);
         this.uoButton.TabIndex = 4;
         this.uoButton.Text = "Uruchom UO";
         this.uoButton.UseVisualStyleBackColor = true;
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(199, 58);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(216, 62);
         this.button1.TabIndex = 5;
         this.button1.Text = "Odwiedz strone WWW";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // downloadProgress
         // 
         this.downloadProgress.Location = new System.Drawing.Point(15, 125);
         this.downloadProgress.Name = "downloadProgress";
         this.downloadProgress.Size = new System.Drawing.Size(400, 12);
         this.downloadProgress.Step = 1;
         this.downloadProgress.TabIndex = 6;
         // 
         // UpdaterForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(427, 167);
         this.Controls.Add(this.downloadProgress);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.uoButton);
         this.Controls.Add(this.statusStrip1);
         this.Controls.Add(this.TitleLabel);
         this.Controls.Add(this.menuStrip1);
         this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MainMenuStrip = this.menuStrip1;
         this.Name = "UpdaterForm";
         this.Text = "DM Loader";
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         this.statusStrip1.ResumeLayout(false);
         this.statusStrip1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem ustawieniaToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
      private System.Windows.Forms.Label TitleLabel;
      private System.Windows.Forms.StatusStrip statusStrip1;
      private System.Windows.Forms.ToolStripStatusLabel statusLabel;
      private System.Windows.Forms.ToolStripProgressBar progressBar;
      private System.Windows.Forms.Button uoButton;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.ProgressBar downloadProgress;
   }
}

