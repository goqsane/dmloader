﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DMLoader.Controllers;

namespace DMLoader
{
   public class SettingsForm : Form
   {

      private AppController _controller;

      private TextBox textBox1;
      private Button btnSave;
      private Button btnBrowse;
      private Label label1;

      public SettingsForm(AppController controller)
      {
         _controller = controller;
         InitializeComponent();
         BindControls();
      }

      private void BindControls()
      {
         textBox1.Text = _controller.Settings.UoPath;
      }

      private bool Validate()
      {
         if (_controller.Settings.IsEmpty()) {
            return false;
         }

         return true;
      }


      private void InitializeComponent()
      {
         this.label1 = new System.Windows.Forms.Label();
         this.textBox1 = new System.Windows.Forms.TextBox();
         this.btnSave = new System.Windows.Forms.Button();
         this.btnBrowse = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.Location = new System.Drawing.Point(12, 21);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(114, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Folder Ultima Online";
         // 
         // textBox1
         // 
         this.textBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.textBox1.Location = new System.Drawing.Point(132, 18);
         this.textBox1.Name = "textBox1";
         this.textBox1.Size = new System.Drawing.Size(453, 22);
         this.textBox1.TabIndex = 1;
         // 
         // btnSave
         // 
         this.btnSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnSave.Location = new System.Drawing.Point(15, 73);
         this.btnSave.Name = "btnSave";
         this.btnSave.Size = new System.Drawing.Size(75, 23);
         this.btnSave.TabIndex = 2;
         this.btnSave.Text = "Zapisz";
         this.btnSave.UseVisualStyleBackColor = true;
         this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
         // 
         // btnBrowse
         // 
         this.btnBrowse.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnBrowse.Location = new System.Drawing.Point(591, 17);
         this.btnBrowse.Name = "btnBrowse";
         this.btnBrowse.Size = new System.Drawing.Size(75, 23);
         this.btnBrowse.TabIndex = 3;
         this.btnBrowse.Text = "...";
         this.btnBrowse.UseVisualStyleBackColor = true;
         this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
         // 
         // SettingsForm
         // 
         this.ClientSize = new System.Drawing.Size(669, 110);
         this.Controls.Add(this.btnBrowse);
         this.Controls.Add(this.btnSave);
         this.Controls.Add(this.textBox1);
         this.Controls.Add(this.label1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
         this.Name = "SettingsForm";
         this.Text = "Ustawienia";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      private void btnBrowse_Click(object sender, EventArgs e)
      {
         var fsd = new FolderBrowserDialog();
         if (fsd.ShowDialog() == DialogResult.Cancel) {
            return;
         }

         textBox1.Text = fsd.SelectedPath;
      }

      private void btnSave_Click(object sender, EventArgs e)
      {

         _controller.Settings.UoPath = textBox1.Text;

         if (!Validate())
         {
            MessageBox.Show(
               "Nie mozna zapisac aktualnych ustawien. Sprawdz, czy wybrany folder jest prawidlowym folderem Ultima Online.",
               "Blad zapisu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
         }

         _controller.Settings.UoPath = textBox1.Text;
         _controller.Settings.Save();

         Close();
      }
   }
}
