﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMLoader.Helpers
{
   [Serializable]
   public class UpdateFileInformation
   {
      public int CurrentFileVersion { get; set; }
      public IList<FileDownloadLine> Downloads { get; set; }
      public DateTime LastUpdatedOn { get; set; }
      public string NewsItemsUri { get; set; }
   }


   [Serializable]
   public class FileDownloadLine
   {
      public string Name { get; set; }
      public string Description { get; set; }
      public string Url { get; set; }
      public bool RequiresUnzipping { get; set; }
      public string CrcHash { get; set; }
   }

   [Serializable]
   public class NewsItemFile
   {
      public DateTime LastUpdated { get; set; }
      public int Version { get; set; }

      public IList<NewsItem> News { get; set; }
   }

   [Serializable]
   public class NewsItem
   {
      public DateTime DateAdded { get; set; }
      public string Title { get; set; }
      public string HTMLContent { get; set; }
   }


}
