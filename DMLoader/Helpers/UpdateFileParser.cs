﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DMLoader.Helpers
{
   public class UpdateFileParser
   {

      public static UpdateFileInformation Get(string fileContents)
      {
         return JsonConvert.DeserializeObject<UpdateFileInformation>(fileContents);
      }
   }
}
