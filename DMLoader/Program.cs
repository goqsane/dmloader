﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DMLoader.Helpers;
using Newtonsoft.Json;

namespace DMLoader
{
   static class Program
   {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main(string[] args) {
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         /*
         if (args.Length == 0) {
           
         }
         else {*/

         UpdateFileInformation info = new UpdateFileInformation();
         info.CurrentFileVersion = new Random().Next(30, 100);
         info.LastUpdatedOn = DateTime.MaxValue;
         info.Downloads = new List<FileDownloadLine>() {
            new FileDownloadLine() {
               CrcHash = "lol",
               Description = "geu",
               Name = "geu.txt",
               RequiresUnzipping = true,
               Url = @"http://dm-uo.pl/download/dm-files-misc.zip"
            }
         };
         string file = JsonConvert.SerializeObject(info);
         File.WriteAllText(@"C:\dm\update.html", file);
      



      Application.Run(new UpdaterForm());
         //}
      }
   }
}
