﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DMLoader.Controllers.Events;
using DMLoader.Helpers;
using DMLoader.Properties;

namespace DMLoader.Controllers
{
   public class AppController {

      public EventHandler DownloadFileDownloadStart;
      public EventHandler DownloadFileDownloadEnd;
      public EventHandler DownloadFileDownloadFail;
      public EventHandler NoUpdatesAvailable;
      public EventHandler CustomToolRan;
      public EventHandler<FileDownloadBeginEventArgs> FileDownloadBegan;
      public EventHandler<FileUnzipEventArgs> FileUnzipped;
      public EventHandler<FileDownloadProgressEventArgs> FileDownloadProgressChanged;



      public readonly WebClient WebClient;
      public AppSettings Settings { get; }

      public AppController()
      {
         Settings = new AppSettings();
         WebClient = new WebClient();
      }

      public bool VerifyConfigurationIntegrity()
      {
         if (Settings.IsEmpty())
         {
            return false;
         }
         return true;
      }

      public void DownloadFile(Uri uri, string destination)
      {
         using (var wc = new WebClient())
         {
            wc.DownloadProgressChanged += WcOnDownloadProgressChanged;
            wc.DownloadFileCompleted += WcOnDownloadFileCompleted;

            var syncObj = new Object();
            lock (syncObj)
            {
               wc.DownloadFileAsync(uri, destination, syncObj);
               //This would block the thread until download completes
               Monitor.Wait(syncObj);
            }
         }

         //Do more stuff after download was complete
      }

      private void WcOnDownloadFileCompleted(object sender, AsyncCompletedEventArgs asyncCompletedEventArgs)
      {
         DownloadFileDownloadEnd?.Invoke(this, EventArgs.Empty);
      }

      private void WcOnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs downloadProgressChangedEventArgs)
      {
         FileDownloadProgressChanged?.Invoke(this, new FileDownloadProgressEventArgs(downloadProgressChangedEventArgs.ProgressPercentage));
      }

      public bool UpdatesAvailable()
      {

         string fullDownloadPath = Path.Combine(Directory.GetCurrentDirectory(), "updateinfo.txt");

         // Need to make sure the file is deleted before we download the update info file.
         if (File.Exists(fullDownloadPath)) {
            File.Delete(fullDownloadPath);
         }

         try
         {
            DownloadFileDownloadStart?.Invoke(this, EventArgs.Empty);
            WebClient.DownloadFile(Settings.UpdateFileUri, fullDownloadPath);
            DownloadFileDownloadEnd?.Invoke(this, EventArgs.Empty);
         }
         catch (Exception ex)
         {
            MessageBox.Show(Resources.Updater_Download_File_Problem);
            DownloadFileDownloadFail?.Invoke(this, EventArgs.Empty);
            return false;
         }

         string fileContents = File.ReadAllText(fullDownloadPath);

         if (String.IsNullOrEmpty(fileContents))
         {
            DownloadFileDownloadFail?.Invoke(this, EventArgs.Empty);
            return false;
         }
         var fi = UpdateFileParser.Get(fileContents);

         if (fi.CurrentFileVersion > Settings.LastUpdateVersion)
         {
            
            return true;
         }

         NoUpdatesAvailable?.Invoke(this, EventArgs.Empty);
         return false;


      }

      public bool DownloadUpdates(UpdateFileInformation information) {

         string patcherPath = Path.Combine(Settings.UoPath, "Patcher");

         if (!Directory.Exists(patcherPath)) {
            Directory.CreateDirectory(patcherPath);
         }

         foreach (var file in information.Downloads) {
            WebClient.DownloadFile(file.Url, Path.Combine(patcherPath, file.Name));
         }

         return true;
      }

      public UpdateFileInformation GetUpdateFile() {

         string fullDownloadPath = Path.Combine(Directory.GetCurrentDirectory(), "updateinfo.txt");

         if (!File.Exists(fullDownloadPath)) {
            return null;
         }

         string fileContents = File.ReadAllText(fullDownloadPath);

         if (String.IsNullOrEmpty(fileContents)) {
            return null;
         }
         var fi = UpdateFileParser.Get(fileContents);
         return fi;
      }


   }
}
