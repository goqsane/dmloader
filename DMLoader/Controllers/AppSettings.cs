﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DMLoader.Properties;
using Newtonsoft.Json;

namespace DMLoader.Controllers
{
   [Serializable]
   public class AppSettings {
      public string UoPath { get; set; }
      public string PatchingUtilityName { get; set; }
      public string UpdateFileUri { get; set; }
      public string UpdaterTitle { get; set; }
      public int LastUpdateVersion { get; set; }
      public string PatchingUtilityInteractions { get; set; }
      public string HttpUri { get; set; }

      public void Save() {
         string currentConfigFile = Path.Combine(Directory.GetCurrentDirectory(), "dm.loader");
         var configFileText = JsonConvert.SerializeObject(this);
         File.WriteAllText(currentConfigFile, configFileText);
      }

      public AppSettings()
      {
         Load();
      }

      public void Load()
      {
         string currentConfigFile = Path.Combine(Directory.GetCurrentDirectory(), "dm.loader");
         if (!File.Exists(currentConfigFile))
         {
            Reset();
         }

         var fileContents = File.ReadAllText(currentConfigFile);

         try {
            dynamic appSettings = JsonConvert.DeserializeObject(fileContents);
            UoPath = appSettings.UoPath;
            LastUpdateVersion = appSettings.LastUpdateVersion;
            PatchingUtilityName = appSettings.PatchingUtilityName;
            UpdateFileUri = appSettings.UpdateFileUri;
            UpdaterTitle = appSettings.UpdaterTitle;
            PatchingUtilityInteractions = appSettings.PatchingUtilityInteractions;
            HttpUri = appSettings.HttpUri;
         }
         catch (Exception ex) {
            Reset();
         }

      }

      public void Reset()
      {
         string currentConfigFile = Path.Combine(Directory.GetCurrentDirectory(), "dm.loader");

         /* First we try to locate dm.loader, if exists we are going to delete it just in case. */
         if (File.Exists(currentConfigFile))
         {
            try
            {
               File.Delete(currentConfigFile);
            }
            catch (IOException ex)
            {
               MessageBox.Show(Resources.Error_DeleteDmLoaderFile_Failed);
            }
         }
         UoPath = @"C:\Program Files\UO";
         PatchingUtilityName = "vinu.exe";
        
         UpdateFileUri = @"http://localhost/update.html";
         LastUpdateVersion = 0;
         UpdaterTitle = "Wiesci ze swiata X";
         HttpUri = "http://tempuri.org";
         PatchingUtilityInteractions = "Y";
         Save();
      }

      public bool IsEmpty()
      {
         if (String.IsNullOrEmpty(UoPath) || String.IsNullOrEmpty(PatchingUtilityName)) {
            return true;
         }

         if (!Directory.Exists(UoPath)) {
            return true;
         }

         if (!File.Exists(Path.Combine(UoPath, "client.exe"))) {
            return true;
         }

         return false;

      }
   }

   
}
