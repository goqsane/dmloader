﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMLoader.Controllers.Events
{
   public class FileDownloadProgressEventArgs : EventArgs
   {

      public int Percentage { get; }

      public FileDownloadProgressEventArgs(int percent) {
         Percentage = percent;
      }
   }
}
