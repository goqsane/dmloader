﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMLoader.Controllers.Events
{
   public class FileUnzipEventArgs : EventArgs
   {
      public string FileName { get; }
      public string FilePath { get; }

      public FileUnzipEventArgs(string fileName, string filePath) {
         FileName = fileName;
         FilePath = filePath;
      }

   }
}
