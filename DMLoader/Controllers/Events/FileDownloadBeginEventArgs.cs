﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMLoader.Controllers.Events
{
   public class FileDownloadBeginEventArgs : EventArgs
   {
      public string FileName { get; }
      public string FileUri { get; }

      public FileDownloadBeginEventArgs(string fileName, string fileUri) {
         FileName = fileName;
         FileUri = fileUri;

      }
   }
}
